package com.projects.forklift.presenter.callback;


public interface CallBackCreateImage {
    void absolutPath(String path);
}
