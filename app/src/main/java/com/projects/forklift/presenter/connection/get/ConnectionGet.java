package com.projects.forklift.presenter.connection.get;

import android.app.Activity;


import com.projects.forklift.presenter.manager.IntentManager;
import com.projects.forklift.presenter.utils.Util;
import com.projects.forklift.presenter.utils.UtilsCodeCheck;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;


@EBean
public class ConnectionGet {

    @RootContext protected Activity activity;

    @Bean protected UtilsCodeCheck utilsCodeCheck;
    @Bean protected IntentManager intentManager;
    @Bean protected Util util;


//    public void getHome(final ProgressDialog progressDialog, final CallbackConnection callbackConnection) {
//        progressDialog.show();
//        final ApiConfig apiConfig = ApiClient.getClient(activity).create(ApiConfig.class);
//        Call<ResponseHome> call = apiConfig.getHome();
//        call.enqueue(new Callback<ResponseHome>() {
//            @Override
//            public void onResponse(Call<ResponseHome> call, Response<ResponseHome> response) {
//                utilsCodeCheck.checkCodeGet(callbackConnection, response, progressDialog);
//            }
//
//            @Override
//            public void onFailure(Call<ResponseHome> call, Throwable t) {
//                util.showDialogError(activity, t.getMessage());
//                progressDialog.dismiss();
//            }
//        });
//    }
//
//
}
