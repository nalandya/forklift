package com.projects.forklift.base_new.presenter.custom.button;

import android.content.Context;
import android.os.Build;
import androidx.annotation.RequiresApi;
import android.util.AttributeSet;
import android.widget.Button;

import com.projects.forklift.base_new.presenter.utils.ReadFont;


public class CustomButtonLight extends Button {
    private ReadFont readFont;

    public CustomButtonLight(Context context) {
        super(context);
        setFont(context);
    }

    public CustomButtonLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont(context);

    }

    public CustomButtonLight(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setFont(context);

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public CustomButtonLight(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setFont(context);

    }


    protected void setFont(Context context) {
        readFont = new ReadFont(context);
        setTypeface(readFont.light());
    }


}
