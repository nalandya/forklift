package com.projects.forklift.presenter.session;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.projects.forklift.model.response.ResponseLogin;

public class SessionLogin {
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private final String sessionName = "login";
    private final String key = "data";

    public SessionLogin(Context context) {
        sharedPreferences = context.getSharedPreferences(sessionName, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public void setData(ResponseLogin data) {
        editor.putString(key, new Gson().toJson(data));
        editor.commit();
    }

    public ResponseLogin getData() {
        String json = sharedPreferences.getString(key, null);
        return new Gson().fromJson(json, ResponseLogin.class);
    }

    public void clearData() {
        editor.clear();
        editor.commit();
    }
}
