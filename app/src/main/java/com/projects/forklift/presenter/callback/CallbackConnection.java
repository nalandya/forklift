package com.projects.forklift.presenter.callback;



public interface CallbackConnection {
    void onSuccess(Object o);
    void onSuccessNull();
}
