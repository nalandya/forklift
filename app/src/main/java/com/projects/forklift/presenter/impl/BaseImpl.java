package com.projects.forklift.presenter.impl;

import android.app.Activity;
import android.app.ProgressDialog;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;

import com.projects.forklift.presenter.callback.CallbackConnection;
import com.projects.forklift.presenter.connection.post.ConnectionPost;
import com.projects.forklift.presenter.manager.DialogManager;
import com.projects.forklift.presenter.manager.IntentManager;
import com.projects.forklift.presenter.utils.FragmentManagerUtils;
import com.projects.forklift.presenter.utils.PermissionMarshmellow;
import com.projects.forklift.presenter.utils.UtilsLayout;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by AndreHF on 3/28/2017.
 */

@EBean
public class BaseImpl<T> implements CallbackConnection {
    @RootContext
    protected Activity activity;

    @Bean
    protected IntentManager intentManager;

    @Setter
    protected T viewAct;
    @Bean
    protected UtilsLayout utilsLayout;
    @Getter
    @Bean
    protected FragmentManagerUtils fragmentManagerUtils;
    @Bean
    protected DialogManager dialogManager;
    @Bean
    protected PermissionMarshmellow permissionMarshmellow;
    @Bean
    protected ConnectionPost connectionPost;
    protected ProgressDialog progressDialog;


    @AfterInject
    protected void inject() {
        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage("Loading");
    }

    public void setFragmentManager(FragmentManager fragmentManager, String className) {
        fragmentManagerUtils.setFragmentManager(fragmentManager);
        fragmentManagerUtils.setCallback(this);
        fragmentManagerUtils.setClassName(className);
        dialogManager.setFragmentManager(fragmentManager);
    }

    public void setLayoutFragment(int layoutFragment) {
        fragmentManagerUtils.setLayoutFragment(layoutFragment);
    }

    @Override
    public void onSuccess(Object o) {

    }

    @Override
    public void onSuccessNull() {

    }

    public void showHidePassword(TextView textView) {
        utilsLayout.showHidePass(textView);
    }


}
