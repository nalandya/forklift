package com.projects.forklift.presenter.callback;

import android.graphics.Bitmap;

public interface CallBackCameraUtils {
    void image(Bitmap bitmap);
}
