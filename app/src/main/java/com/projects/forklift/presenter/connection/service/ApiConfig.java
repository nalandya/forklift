package com.projects.forklift.presenter.connection.service;



import com.projects.forklift.model.request.RequestLogin;
import com.projects.forklift.model.response.ResponseLogin;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by faizf on 2/8/2017.
 */

public interface ApiConfig {

    @POST("login")
    Call<ResponseLogin> doLogin(@Body RequestLogin requestLogin);

//    @GET("cities/{id}/districts")
//    Call<ResponseListDistrict> getListDistrict(@Path("id") int id);

//    @Multipart
//    @POST("images/upload")
//    Call<ResponseUploadPicture> doUploadImageUmkm(@Part MultipartBody.Part part, @Query("path") String path);
}
