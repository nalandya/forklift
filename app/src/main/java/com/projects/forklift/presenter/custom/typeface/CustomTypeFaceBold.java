package com.projects.forklift.base_new.presenter.custom.typeface;

import android.content.Context;


public class CustomTypeFaceBold extends BaseCustomTypeFace {
    public CustomTypeFaceBold(Context context) {
        super(context);
        typeface = readFont.bold();
    }
}
