package com.projects.forklift.presenter.connection.post;

import android.app.Activity;
import android.app.ProgressDialog;

import com.projects.forklift.model.request.RequestLogin;
import com.projects.forklift.model.response.ResponseLogin;
import com.projects.forklift.presenter.callback.CallbackConnection;
import com.projects.forklift.presenter.connection.service.ApiClient;
import com.projects.forklift.presenter.connection.service.ApiConfig;
import com.projects.forklift.presenter.manager.IntentManager;
import com.projects.forklift.presenter.utils.Util;
import com.projects.forklift.presenter.utils.UtilsCodeCheck;
import com.projects.forklift.presenter.utils.UtilsLayout;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


@EBean
public class ConnectionPost {

    @RootContext
    protected Activity activity;
    @Bean
    protected IntentManager intentManager;
    @Bean
    protected Util util;
    @Bean
    protected UtilsLayout utilsLayout;
    @Bean
    protected UtilsCodeCheck utilsCodeCheck;

    public void goLogin(final ProgressDialog progressDialog, RequestLogin requestLogin, final CallbackConnection callbackConnection) {
        progressDialog.show();
        final ApiConfig apiConfig = ApiClient.getClient(activity).create(ApiConfig.class);
        Call<ResponseLogin> loginCall = apiConfig.doLogin(requestLogin);
        loginCall.enqueue(new Callback<ResponseLogin>() {
            @Override
            public void onResponse(Call<ResponseLogin> call, Response<ResponseLogin> response) {
                utilsCodeCheck.checkCodeGet(callbackConnection, response, progressDialog);
            }

            @Override
            public void onFailure(Call<ResponseLogin> call, Throwable t) {
                util.showDialogError(activity, t.getMessage());
                progressDialog.dismiss();

            }
        });
    }


}
