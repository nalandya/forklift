package com.projects.forklift.view.activity.login;


import com.projects.forklift.model.request.RequestLogin;
import com.projects.forklift.model.response.ResponseLogin;
import com.projects.forklift.presenter.impl.BaseImpl;
import com.projects.forklift.presenter.session.SessionLogin;

import org.androidannotations.annotations.EBean;

@EBean
public class LoginImpl extends BaseImpl<LoginContract.View> implements LoginContract.Presenter{

    @Override
    public void doLogin(String username, String password) {
        RequestLogin requestLogin = new RequestLogin();
        requestLogin.setUsername(username);
        requestLogin.setPassword(password);
        connectionPost.goLogin(progressDialog, requestLogin, this);
    }

    @Override
    public void checkLogin() {
        if (new SessionLogin(activity).getData() != null) {

        }
    }

    @Override
    public void onSuccess(Object o) {
        super.onSuccess(o);
        if (o instanceof ResponseLogin) {
            new SessionLogin(activity).setData((ResponseLogin) o);
        }
    }

}
