package com.projects.forklift.view.activity.home;

import com.projects.forklift.presenter.impl.BaseImpl;

import org.androidannotations.annotations.EBean;

@EBean
public class HomeImpl extends BaseImpl<HomeContract.View> implements HomeContract.Presenter {
}
