package com.projects.forklift.view.activity.login;

public interface LoginContract {
    interface View{

    }

    interface Presenter{

        void doLogin(String bodyText, String bodyText1);

        void checkLogin();
    }
}
