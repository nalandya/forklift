package com.projects.forklift.view.activity.home;

import com.projects.forklift.R;
import com.projects.forklift.presenter.base.activity.BaseActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;

@EActivity(R.layout.activity_home)
public class HomeActivity extends BaseActivity implements HomeContract.View {

    @Bean HomeImpl impl;

    @AfterViews
    protected void init() {

    }

}
