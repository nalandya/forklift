package com.projects.forklift.view.activity.login;

import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.projects.forklift.R;
import com.projects.forklift.presenter.base.activity.BaseActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Touch;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_login)
public class LoginActivity extends BaseActivity implements LoginContract.View {

    @Bean
    LoginImpl login;
    @ViewById
    EditText username;
    @ViewById
    EditText password;

    @AfterViews
    protected void init() {
        login.setViewAct(this);
        login.checkLogin();
    }

    @Click
    protected void login() {
//        login.doLogin(utilsLayout.getBodyText(username), utilsLayout.getBodyText(password));
    }

    @Touch(R.id.password)
    protected boolean touch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP && v instanceof EditText) {
            if (event.getRawX() >= (v.getRight() - ((EditText) v).getCompoundDrawables()[2]
                    .getBounds().width())) {
                login.showHidePassword((TextView) v);
                return true;
            }
        }
        return false;
    }

}
