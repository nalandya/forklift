package com.projects.forklift.model.request;

import lombok.Data;

@Data
public class RequestLogin {
    private String username;
    private String password;
}
