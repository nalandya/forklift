package com.projects.forklift.model.request;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import lombok.Data;

@Data
public class ModelListCheckList {
    private String nama;
    @SerializedName("id_checklist")
    private int idChecklist;
    @SerializedName("tgl_chechklist")
    private Date tglChecklist;
    private String kondisi;
    private String keterangan;
}
