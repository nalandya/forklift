package com.projects.forklift.model.request;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class RequestChecklist {
    @SerializedName("id_petugas")
    private int idPetugas;
    @SerializedName("id_forklift")
    private int idForklift;
    private int kebersihan;
    @SerializedName("tuas_kontrol")
    private int tuasKontrol;
    @SerializedName("garpu_bracket")
    private int garpuBracket;
    private int sirine;
    private int battery;
    @SerializedName("socket_battery")
    private int socketBattery;
    private int roda;
    private int body;
    private int rem;
    @SerializedName("selang_hidrolik")
    private int selangHidrolik;
    @SerializedName("lampu_sirine")
    private int lampuSirine;
    private int oil;
    @SerializedName("roda_kemudi")
    private int rodaKemudi;
    @SerializedName("rantai_baut_pengikat")
    private int rantaiBautPengikat;
    private int apar;
    @SerializedName("lampu_indikator")
    private int lampuIndikator;
    private String keterangan;

}
