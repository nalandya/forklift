package com.projects.forklift.model.response;

import lombok.Data;

@Data
public class ResponseLogin {
    private int id;
    private String name;
    private String type;
}
