package com.projects.forklift.model.response;

import com.projects.forklift.model.request.ModelListCheckList;

import lombok.Data;

@Data
public class ResponseGetChecklist {
    private ModelListCheckList[] data;
}
