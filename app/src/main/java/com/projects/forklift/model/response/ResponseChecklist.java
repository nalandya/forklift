package com.projects.forklift.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import lombok.Data;

@Data
public class ResponseChecklist {
    @SerializedName("id_checklist")
    private int idChecklist;
    @SerializedName("tgl_checklist")
    private Date date;
}
