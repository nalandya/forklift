package com.projects.forklift.base_new.model.response;

import java.io.Serializable;

import lombok.Data;


@Data
public class BaseResponse<DATA, ERROR> implements Serializable {
    private int status;
    private String path;
    private String message;
    private String[] errors;
    private String newAuthToken;
    private ERROR error;
    DATA data;
}
